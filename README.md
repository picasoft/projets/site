# Site Picasoft

Le nouveau site de Picasoft

Fait avec Zola et TailwindCSS.

Sur la base de [cette vue Figma](https://www.figma.com/file/C9Vdq1fuDRnUgpQMr75pR4/%5BWIP%5D-PICASOFT?type=design&node-id=101-254).

# Installation

Besoin de Zola et de Nodejs et npm/yarn/...

Faire un `npm i` ou un `yarn` ou whatever.

# Compilation

C'est un peu crado mais je lance dans 2 terminaux différents :

```bash
$ zola serve
```
```bash
$ while sleep 2; do zola build; npx tailwindcss -i ./css/main.css -o ./static/css/main.css; done
```

Le problème de `zola serve` seul, c'est qu'il ne crée pas les fichiers dans `./public`, dont `tailwindcss` se sert pour générer son CSS.
