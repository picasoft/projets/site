with import <nixpkgs> {};

let
in
pkgs.mkShell {
  name = "rust-tools";
  buildInputs = [
    zola
    nodePackages.tailwindcss
  ];
  src = null;
  shellHook = ''
  '';
}

